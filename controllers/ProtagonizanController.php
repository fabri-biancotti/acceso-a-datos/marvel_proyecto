<?php

namespace app\controllers;

use Yii;
use app\models\Protagonizan;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProtagonizanController implements the CRUD actions for Protagonizan model.
 */
class ProtagonizanController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Protagonizan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Protagonizan::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Protagonizan model.
     * @param string $dni_actor
     * @param string $cod_pelicula
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($dni_actor, $cod_pelicula)
    {
        return $this->render('view', [
            'model' => $this->findModel($dni_actor, $cod_pelicula),
        ]);
    }

    /**
     * Creates a new Protagonizan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Protagonizan();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'dni_actor' => $model->dni_actor, 'cod_pelicula' => $model->cod_pelicula]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Protagonizan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $dni_actor
     * @param string $cod_pelicula
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($dni_actor, $cod_pelicula)
    {
        $model = $this->findModel($dni_actor, $cod_pelicula);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'dni_actor' => $model->dni_actor, 'cod_pelicula' => $model->cod_pelicula]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Protagonizan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $dni_actor
     * @param string $cod_pelicula
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($dni_actor, $cod_pelicula)
    {
        $this->findModel($dni_actor, $cod_pelicula)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Protagonizan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $dni_actor
     * @param string $cod_pelicula
     * @return Protagonizan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($dni_actor, $cod_pelicula)
    {
        if (($model = Protagonizan::findOne(['dni_actor' => $dni_actor, 'cod_pelicula' => $cod_pelicula])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
