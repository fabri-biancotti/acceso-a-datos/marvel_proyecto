<?php

namespace app\controllers;

use Yii;
use app\models\Estreno;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * EstrenoController implements the CRUD actions for Estreno model.
 */
class EstrenoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Estreno models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Estreno::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Estreno model.
     * @param string $cod_espectador
     * @param string $cod_pelicula
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($cod_espectador, $cod_pelicula)
    {
        return $this->render('view', [
            'model' => $this->findModel($cod_espectador, $cod_pelicula),
        ]);
    }

    /**
     * Creates a new Estreno model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Estreno();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'cod_espectador' => $model->cod_espectador, 'cod_pelicula' => $model->cod_pelicula]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Estreno model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $cod_espectador
     * @param string $cod_pelicula
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($cod_espectador, $cod_pelicula)
    {
        $model = $this->findModel($cod_espectador, $cod_pelicula);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'cod_espectador' => $model->cod_espectador, 'cod_pelicula' => $model->cod_pelicula]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Estreno model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $cod_espectador
     * @param string $cod_pelicula
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($cod_espectador, $cod_pelicula)
    {
        $this->findModel($cod_espectador, $cod_pelicula)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Estreno model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $cod_espectador
     * @param string $cod_pelicula
     * @return Estreno the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($cod_espectador, $cod_pelicula)
    {
        if (($model = Estreno::findOne(['cod_espectador' => $cod_espectador, 'cod_pelicula' => $cod_pelicula])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
