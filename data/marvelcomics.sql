-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 27-11-2019 a las 10:32:42
-- Versión del servidor: 10.1.38-MariaDB
-- Versión de PHP: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `marvelcomics`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `actores`
--

CREATE TABLE `actores` (
  `dni_actor` varchar(9) NOT NULL,
  `nombre` varchar(35) NOT NULL,
  `personaje` varchar(35) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `actores`
--

INSERT INTO `actores` (`dni_actor`, `nombre`, `personaje`) VALUES
('00', 'WCC94WA7S7', 'J'),
('0JE', 'J041U61', '61M'),
('2Q', '5XVJ114IN5N5TX32M', '57Z68670F2B'),
('3', 'NM99OGB57T0', '6UH'),
('3LQ', '3S7JZC5016J29XU287', 'A80'),
('4X3', 'VA8N5LWNPN', '2V4I3HJ1'),
('507C0262', '7', 'B19Z'),
('52', '6ZADG8', 'DTB'),
('5I', '1K9B7192931C5', '5W8I1ZC4'),
('7', '1848Q244M6', 'XBB720'),
('74Y7', '8', 'P7PA33Z2C'),
('7R', '5CJ', 'Y0E'),
('80X', '43208ADSK90R7TA8J7F1B9Z26Z41K16G4H', 'C568N45PIGRU7X'),
('848', 'J6O3', 'E7UKXI0'),
('8S', 'N', 'W'),
('970G', 'JZY88098W937ISU648FJ9AGBQ7GY16Y4', '87'),
('9DL', 'V8D', '50SNUKX6XZ844H53'),
('A', '0', 'K1'),
('CC5', '810V', '2'),
('D', '0Z569M88', 'WQ19D2VY'),
('E', 'WJQ6TV8QB', 'B39F0RPBC'),
('E8', 'K11Z5WU56W004GA8YD', '1K7'),
('H', 'U98056Q', 'J7G'),
('I15W457', '8G8H737987', '2K742SJG1'),
('JQO10', '6UL3M29I69UV592K5OF0AD45', '92VE56S94S7QQ7S00'),
('K', '9DFL85', '2049'),
('M', 'M', 'D3WPF1'),
('NO4', '81', 'K9W'),
('P', 'NXVP', '0'),
('R7', '6K868LQ07I2OL8V7KG', '206'),
('T', '9A', '3Q6830846YQV498J'),
('UQ74', 'U77823H', 'DT3589R8A'),
('W', 'P', '9C3L1XC'),
('X', '1027HEV1F47A95B5BNIL10U9M5186E4J6', '31M43'),
('XA', 'J604NYT69A6008', 'U');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `consumen`
--

CREATE TABLE `consumen` (
  `cod_espectador` varchar(3) NOT NULL,
  `cod_pelicula` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `directores`
--

CREATE TABLE `directores` (
  `dni_director` varchar(9) NOT NULL,
  `nombre` varchar(35) NOT NULL,
  `telefono` varchar(13) NOT NULL,
  `sueldo` float(7,3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `directores`
--

INSERT INTO `directores` (`dni_director`, `nombre`, `telefono`, `sueldo`) VALUES
('04884', '811N9VJT', '09', -3304.723),
('0K1', '3S4PV2MVB167A0212L8S2ZN', '1O89SZ072', -92.253),
('0S0Y', '8', 'NG16Q8', -7273.096),
('1', 'ZP94J8', 'U', 5759.074),
('178', 'KTHUY5MU64S1L', 'QI', -1511.157),
('2', '2', '9', 9552.367),
('22', 'D', 'S7FKB6C5F0BH7', 2588.184),
('2P', 'PR0', 'JCVUK0699HRP5', -3985.575),
('3', '779D6FRASSGZH', 'KHJ', 8408.096),
('3D', 'L3W92590LF2G6', 'D', -258.473),
('3L', '54E9', '6', -8206.884),
('60', 'F45O6NYY298OA', '22U3', -5701.276),
('7', 'ZA9K25FD2356R3PTI3P', 'FK8X093O5A829', -8930.092),
('77SO5H', '2B3', '27M9', -2100.215),
('77YL6', 'ZJ72', 'FS3CO7BN99TM', 2045.067),
('7P', 'TNLK68WQ5', 'U38KCE9K2XVQX', -5236.424),
('7Q3', '170', '3B3F1RQJI', -8775.236),
('8', '725D', 'ZA38IY', 3070.202),
('8Q1', 'H52', '2433GN4Y7179', -1121.241),
('9', '5STBBW3AC', '0T50U063', 7441.043),
('BY9', '3HZ8M5T30ZBVXQ1E', 'FLF8Z181OJ', 5453.363),
('C2', 'O2MO7P280128A1U', 'G', -4198.948),
('CSE657IHK', '2345', 'F7E0M84', 8843.453),
('CV6', 'B08', '4', 3111.723),
('DQ41Y', 'E', 'GZ539A01JDEW8', 6013.604),
('E93H71', '75R1S9KU960HM320', 'Z', -8449.213),
('GT52', 'AG', 'G', 5054.968),
('L', 'K18T', '1ZP', -8022.955),
('N', 'GJ362T', 'QM9YDQ3V9', 4315.195),
('N2C', '19TRJ8HNA9AEA0', 'P', 7516.529),
('QYL881', 'Q71R127Y96A0CED6HJ1Y5', '9', 1303.315),
('SHCG', 'UEL', '418Y8', 9908.226),
('U1', 'VKHR0O0', 'QLMV', 372.061),
('X', 'CMW0ZB55584', 'VFO', -1.688),
('YI3T', '5S26J20', '837Q5', 7832.533);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `espectadores`
--

CREATE TABLE `espectadores` (
  `cod_espectador` varchar(3) NOT NULL,
  `nombre` varchar(35) NOT NULL,
  `pais` varchar(50) NOT NULL,
  `nombre_cine` varchar(35) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `espectadores`
--

INSERT INTO `espectadores` (`cod_espectador`, `nombre`, `pais`, `nombre_cine`) VALUES
('0IB', 'O5XZ78CKFJ', 'B0U3R0DOKRR074TRXC9467UOND3X62U5', 'G5'),
('0IM', '83X5NWW3YBJ9', 'MQDM44H', 'J2Z4W71W7F1W9U'),
('0T1', 'V', '81D3VAL56YV2VXRZ8KGQ8M6T2J1C9WWS4K093L2NKRXHV61OAB', '4'),
('10N', '66B2', 'SUG9SMM68WF7896F1787O09UYIMRHZ365GK991AGUV59978SXW', 'T6ZIIV08'),
('11D', 'E0QATJ', 'WO45SNT4H3RE6UJ8KPU9181E4SX137512295T99X4N790H0011', '5B871IOOWU1Z877SI8P3ST9149G4S'),
('177', '5AD', 'TE0A39OMUX8BF1P955D453X649D59609XDKJ5132', '250S2'),
('3', 'F201D4OFXJ8QSD', 'Q0', '81K27080VO'),
('30Q', '6A', '9HGV4HLN30V54SO29L7292MP44G9NRJ01H114521I0SV3Q9H', '4X3R1299H48JP0'),
('38', '1CIYIOBTYR2W71', 'A22OD1H474409I0X66KG5J7U1UX1S', 'XY'),
('3Z', 'V59', 'O4TO', '24'),
('491', '73J', 'EC022KWNHZ2B480VE8', '4MGPK'),
('51W', 'W4E02K', 'S1C4363HFES', '0SMJ67'),
('56P', 'EQ3SJSD6N', '40', 'G85C0KEZO'),
('6', '7', '7RF2J2694E853', '98'),
('63L', '3', 'M1CVHS7W13363TXY9UP105HM6', '2TK65Z'),
('70', '4N4Z5Y79X8MU6QAS', 'L5D07WIFDRQ5JZRQ2UF79', '8RT33NCC7R1O68'),
('71J', 'GM', 'P3Q602PY1L5EC6F23B4RU01ST51F351UX41', 'K'),
('7E1', 'V2U', 'QUV41J2', 'T0N38R'),
('820', '7SAZ5477', '50B8B1DM6CT5C84DVQHIS57Z5KM39X159D', 'T12'),
('863', '0EMX4', 'V6M', 'K80VQ9IPA1WHF157'),
('9', '35', '6IH9', '5YO7Q'),
('ATP', 'S13E19', '4JOSEZEF4CK86EDHN8XZ00T86T2NBT5O9', 'KQJNZK0JE09'),
('D0Z', '5960QCUW4W', '1ICEL192ONZYM', 'H'),
('E', 'V', '0Q81B2OK48500AW61W636Q7405607UUXIL0', 'LK2AJ0F'),
('IBF', '452KL2M5OFENT7S5MMJ', '2', '05G6I7U6TK'),
('J', '5120', '7H6RQB0F9B487', 'WG0J8'),
('JK9', 'F079CSXXP70U55D', '161', '20KX6'),
('MU5', '6HG0C6EGCRF63GS6', 'AL0U919W83A2ATXIN3HN9ZEA4ZK76OUE0I07P9354739RML287', 'FET93HEZX5EJ5D5IHPVY94JBU1Y'),
('Q91', 'B2', 'D5ONQGM7Z50H2EAT2', 'SG414OSKF22'),
('RIE', 'XWCZ', '2QG669YM40Z', 'MR4'),
('S20', '5', '77R970G2UHQ358NZ7OHCX25FVMBG38721ST7GL35JP8W661WJX', '6YH897'),
('TE', 'PJ6', '14619LD71NTF99J68DG168HG', '18B8'),
('XK8', 'W', '4AN2B3M8652SUG95VVIQY93H', 'UG26Y9KX98'),
('Z9', '966N', '0B5VI02389RR0KOD623UTI856H2US70I2Z01H76810', '33DMMTDCRO53'),
('ZC', '8PKVHXDHL82314V94', 'OT8L0LY3C42K50HJPQM1YJ31P80', 'Z');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estreno`
--

CREATE TABLE `estreno` (
  `cod_espectador` varchar(3) NOT NULL,
  `cod_pelicula` varchar(3) NOT NULL,
  `fecha_estreno` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `peliculas`
--

CREATE TABLE `peliculas` (
  `cod_pelicula` varchar(3) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `presupuesto` float(8,3) NOT NULL,
  `tamano_elenco` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `protagonizan`
--

CREATE TABLE `protagonizan` (
  `dni_actor` varchar(9) NOT NULL,
  `cod_pelicula` varchar(3) NOT NULL,
  `sueldo_actor` float(7,3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `actores`
--
ALTER TABLE `actores`
  ADD PRIMARY KEY (`dni_actor`);

--
-- Indices de la tabla `consumen`
--
ALTER TABLE `consumen`
  ADD PRIMARY KEY (`cod_espectador`,`cod_pelicula`),
  ADD KEY `FK_peli` (`cod_pelicula`);

--
-- Indices de la tabla `directores`
--
ALTER TABLE `directores`
  ADD PRIMARY KEY (`dni_director`);

--
-- Indices de la tabla `espectadores`
--
ALTER TABLE `espectadores`
  ADD PRIMARY KEY (`cod_espectador`);

--
-- Indices de la tabla `estreno`
--
ALTER TABLE `estreno`
  ADD PRIMARY KEY (`cod_espectador`,`cod_pelicula`),
  ADD KEY `FK_peliculas` (`cod_pelicula`);

--
-- Indices de la tabla `peliculas`
--
ALTER TABLE `peliculas`
  ADD PRIMARY KEY (`cod_pelicula`);

--
-- Indices de la tabla `protagonizan`
--
ALTER TABLE `protagonizan`
  ADD PRIMARY KEY (`dni_actor`,`cod_pelicula`),
  ADD KEY `FK_pelis` (`cod_pelicula`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `consumen`
--
ALTER TABLE `consumen`
  ADD CONSTRAINT `FK_espec` FOREIGN KEY (`cod_espectador`) REFERENCES `espectadores` (`cod_espectador`),
  ADD CONSTRAINT `FK_peli` FOREIGN KEY (`cod_pelicula`) REFERENCES `peliculas` (`cod_pelicula`);

--
-- Filtros para la tabla `estreno`
--
ALTER TABLE `estreno`
  ADD CONSTRAINT `FK_espectador` FOREIGN KEY (`cod_espectador`) REFERENCES `espectadores` (`cod_espectador`),
  ADD CONSTRAINT `FK_peliculas` FOREIGN KEY (`cod_pelicula`) REFERENCES `peliculas` (`cod_pelicula`);

--
-- Filtros para la tabla `peliculas`
--
ALTER TABLE `peliculas`
  ADD CONSTRAINT `FK_director_peliculas` FOREIGN KEY (`cod_pelicula`) REFERENCES `directores` (`dni_director`);

--
-- Filtros para la tabla `protagonizan`
--
ALTER TABLE `protagonizan`
  ADD CONSTRAINT `FK_actor` FOREIGN KEY (`dni_actor`) REFERENCES `actores` (`dni_actor`),
  ADD CONSTRAINT `FK_pelis` FOREIGN KEY (`cod_pelicula`) REFERENCES `peliculas` (`cod_pelicula`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
