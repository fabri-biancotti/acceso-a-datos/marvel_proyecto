<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "actores".
 *
 * @property string $dni_actor
 * @property string $nombre
 * @property string $personaje
 *
 * @property Protagonizan[] $protagonizans
 * @property Peliculas[] $codPeliculas
 */
class Actores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'actores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dni_actor', 'nombre', 'personaje'], 'required'],
            [['dni_actor'], 'string', 'max' => 9],
            [['nombre', 'personaje'], 'string', 'max' => 35],
            [['dni_actor'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'dni_actor' => 'Dni Actor',
            'nombre' => 'Nombre',
            'personaje' => 'Personaje',
        ];
    }

    /**
     * Gets query for [[Protagonizans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProtagonizans()
    {
        return $this->hasMany(Protagonizan::className(), ['dni_actor' => 'dni_actor']);
    }

    /**
     * Gets query for [[CodPeliculas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodPeliculas()
    {
        return $this->hasMany(Peliculas::className(), ['cod_pelicula' => 'cod_pelicula'])->viaTable('protagonizan', ['dni_actor' => 'dni_actor']);
    }
}
