<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "estreno".
 *
 * @property string $cod_espectador
 * @property string $cod_pelicula
 * @property string $fecha_estreno
 *
 * @property Espectadores $codEspectador
 * @property Peliculas $codPelicula
 */
class Estreno extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'estreno';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_espectador', 'cod_pelicula', 'fecha_estreno'], 'required'],
            [['fecha_estreno'], 'safe'],
            [['cod_espectador', 'cod_pelicula'], 'string', 'max' => 3],
            [['cod_espectador', 'cod_pelicula'], 'unique', 'targetAttribute' => ['cod_espectador', 'cod_pelicula']],
            [['cod_espectador'], 'exist', 'skipOnError' => true, 'targetClass' => Espectadores::className(), 'targetAttribute' => ['cod_espectador' => 'cod_espectador']],
            [['cod_pelicula'], 'exist', 'skipOnError' => true, 'targetClass' => Peliculas::className(), 'targetAttribute' => ['cod_pelicula' => 'cod_pelicula']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod_espectador' => 'Cod Espectador',
            'cod_pelicula' => 'Cod Pelicula',
            'fecha_estreno' => 'Fecha Estreno',
        ];
    }

    /**
     * Gets query for [[CodEspectador]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodEspectador()
    {
        return $this->hasOne(Espectadores::className(), ['cod_espectador' => 'cod_espectador']);
    }

    /**
     * Gets query for [[CodPelicula]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodPelicula()
    {
        return $this->hasOne(Peliculas::className(), ['cod_pelicula' => 'cod_pelicula']);
    }
}
