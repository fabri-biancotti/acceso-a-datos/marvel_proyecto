<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "protagonizan".
 *
 * @property string $dni_actor
 * @property string $cod_pelicula
 * @property float $sueldo_actor
 *
 * @property Actores $dniActor
 * @property Peliculas $codPelicula
 */
class Protagonizan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'protagonizan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dni_actor', 'cod_pelicula', 'sueldo_actor'], 'required'],
            [['sueldo_actor'], 'number'],
            [['dni_actor'], 'string', 'max' => 9],
            [['cod_pelicula'], 'string', 'max' => 3],
            [['dni_actor', 'cod_pelicula'], 'unique', 'targetAttribute' => ['dni_actor', 'cod_pelicula']],
            [['dni_actor'], 'exist', 'skipOnError' => true, 'targetClass' => Actores::className(), 'targetAttribute' => ['dni_actor' => 'dni_actor']],
            [['cod_pelicula'], 'exist', 'skipOnError' => true, 'targetClass' => Peliculas::className(), 'targetAttribute' => ['cod_pelicula' => 'cod_pelicula']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'dni_actor' => 'Dni Actor',
            'cod_pelicula' => 'Cod Pelicula',
            'sueldo_actor' => 'Sueldo Actor',
        ];
    }

    /**
     * Gets query for [[DniActor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDniActor()
    {
        return $this->hasOne(Actores::className(), ['dni_actor' => 'dni_actor']);
    }

    /**
     * Gets query for [[CodPelicula]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodPelicula()
    {
        return $this->hasOne(Peliculas::className(), ['cod_pelicula' => 'cod_pelicula']);
    }
}
