<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "directores".
 *
 * @property string $dni_director
 * @property string $nombre
 * @property string $telefono
 * @property float $sueldo
 *
 * @property Peliculas $peliculas
 */
class Directores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'directores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dni_director', 'nombre', 'telefono', 'sueldo'], 'required'],
            [['sueldo'], 'number'],
            [['dni_director'], 'string', 'max' => 9],
            [['nombre'], 'string', 'max' => 35],
            [['telefono'], 'string', 'max' => 13],
            [['dni_director'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'dni_director' => 'Dni Director',
            'nombre' => 'Nombre',
            'telefono' => 'Telefono',
            'sueldo' => 'Sueldo',
        ];
    }

    /**
     * Gets query for [[Peliculas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPeliculas()
    {
        return $this->hasOne(Peliculas::className(), ['cod_pelicula' => 'dni_director']);
    }
}
