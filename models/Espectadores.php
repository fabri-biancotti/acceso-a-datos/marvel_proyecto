<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "espectadores".
 *
 * @property string $cod_espectador
 * @property string $nombre
 * @property string $pais
 * @property string $nombre_cine
 *
 * @property Consumen[] $consumens
 * @property Peliculas[] $codPeliculas
 * @property Estreno[] $estrenos
 * @property Peliculas[] $codPeliculas0
 */
class Espectadores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'espectadores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_espectador', 'nombre', 'pais', 'nombre_cine'], 'required'],
            [['cod_espectador'], 'string', 'max' => 3],
            [['nombre', 'nombre_cine'], 'string', 'max' => 35],
            [['pais'], 'string', 'max' => 50],
            [['cod_espectador'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod_espectador' => 'Cod Espectador',
            'nombre' => 'Nombre',
            'pais' => 'Pais',
            'nombre_cine' => 'Nombre Cine',
        ];
    }

    /**
     * Gets query for [[Consumens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getConsumens()
    {
        return $this->hasMany(Consumen::className(), ['cod_espectador' => 'cod_espectador']);
    }

    /**
     * Gets query for [[CodPeliculas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodPeliculas()
    {
        return $this->hasMany(Peliculas::className(), ['cod_pelicula' => 'cod_pelicula'])->viaTable('consumen', ['cod_espectador' => 'cod_espectador']);
    }

    /**
     * Gets query for [[Estrenos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEstrenos()
    {
        return $this->hasMany(Estreno::className(), ['cod_espectador' => 'cod_espectador']);
    }

    /**
     * Gets query for [[CodPeliculas0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodPeliculas0()
    {
        return $this->hasMany(Peliculas::className(), ['cod_pelicula' => 'cod_pelicula'])->viaTable('estreno', ['cod_espectador' => 'cod_espectador']);
    }
}
