<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "peliculas".
 *
 * @property string $cod_pelicula
 * @property string $nombre
 * @property float $presupuesto
 * @property int $tamano_elenco
 *
 * @property Consumen[] $consumens
 * @property Espectadores[] $codEspectadors
 * @property Estreno[] $estrenos
 * @property Espectadores[] $codEspectadors0
 * @property Directores $codPelicula
 * @property Protagonizan[] $protagonizans
 * @property Actores[] $dniActors
 */
class Peliculas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'peliculas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_pelicula', 'nombre', 'presupuesto', 'tamano_elenco'], 'required'],
            [['presupuesto'], 'number'],
            [['tamano_elenco'], 'integer'],
            [['cod_pelicula'], 'string', 'max' => 3],
            [['nombre'], 'string', 'max' => 50],
            [['cod_pelicula'], 'unique'],
            [['cod_pelicula'], 'exist', 'skipOnError' => true, 'targetClass' => Directores::className(), 'targetAttribute' => ['cod_pelicula' => 'dni_director']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod_pelicula' => 'Cod Pelicula',
            'nombre' => 'Nombre',
            'presupuesto' => 'Presupuesto',
            'tamano_elenco' => 'Tamano Elenco',
        ];
    }

    /**
     * Gets query for [[Consumens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getConsumens()
    {
        return $this->hasMany(Consumen::className(), ['cod_pelicula' => 'cod_pelicula']);
    }

    /**
     * Gets query for [[CodEspectadors]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodEspectadors()
    {
        return $this->hasMany(Espectadores::className(), ['cod_espectador' => 'cod_espectador'])->viaTable('consumen', ['cod_pelicula' => 'cod_pelicula']);
    }

    /**
     * Gets query for [[Estrenos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEstrenos()
    {
        return $this->hasMany(Estreno::className(), ['cod_pelicula' => 'cod_pelicula']);
    }

    /**
     * Gets query for [[CodEspectadors0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodEspectadors0()
    {
        return $this->hasMany(Espectadores::className(), ['cod_espectador' => 'cod_espectador'])->viaTable('estreno', ['cod_pelicula' => 'cod_pelicula']);
    }

    /**
     * Gets query for [[CodPelicula]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodPelicula()
    {
        return $this->hasOne(Directores::className(), ['dni_director' => 'cod_pelicula']);
    }

    /**
     * Gets query for [[Protagonizans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProtagonizans()
    {
        return $this->hasMany(Protagonizan::className(), ['cod_pelicula' => 'cod_pelicula']);
    }

    /**
     * Gets query for [[DniActors]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDniActors()
    {
        return $this->hasMany(Actores::className(), ['dni_actor' => 'dni_actor'])->viaTable('protagonizan', ['cod_pelicula' => 'cod_pelicula']);
    }
}
