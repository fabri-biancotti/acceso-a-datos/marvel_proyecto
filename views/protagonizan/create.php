<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Protagonizan */

$this->title = 'Create Protagonizan';
$this->params['breadcrumbs'][] = ['label' => 'Protagonizans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="protagonizan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
