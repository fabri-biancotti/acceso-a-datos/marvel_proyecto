<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Protagonizan */

$this->title = 'Update Protagonizan: ' . $model->dni_actor;
$this->params['breadcrumbs'][] = ['label' => 'Protagonizans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->dni_actor, 'url' => ['view', 'dni_actor' => $model->dni_actor, 'cod_pelicula' => $model->cod_pelicula]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="protagonizan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
