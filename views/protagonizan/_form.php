<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Protagonizan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="protagonizan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'dni_actor')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cod_pelicula')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sueldo_actor')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
