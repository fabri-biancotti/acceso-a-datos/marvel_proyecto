<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Protagonizans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="protagonizan-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Protagonizan', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'dni_actor',
            'cod_pelicula',
            'sueldo_actor',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
