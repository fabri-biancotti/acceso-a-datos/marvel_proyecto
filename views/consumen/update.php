<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Consumen */

$this->title = 'Update Consumen: ' . $model->cod_espectador;
$this->params['breadcrumbs'][] = ['label' => 'Consumens', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cod_espectador, 'url' => ['view', 'cod_espectador' => $model->cod_espectador, 'cod_pelicula' => $model->cod_pelicula]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="consumen-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
