<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Consumen */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="consumen-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cod_espectador')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cod_pelicula')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
