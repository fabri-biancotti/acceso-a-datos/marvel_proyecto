<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Consumen */

$this->title = 'Create Consumen';
$this->params['breadcrumbs'][] = ['label' => 'Consumens', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="consumen-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
