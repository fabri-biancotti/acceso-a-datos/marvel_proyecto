<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Estreno */

$this->title = $model->cod_espectador;
$this->params['breadcrumbs'][] = ['label' => 'Estrenos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="estreno-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'cod_espectador' => $model->cod_espectador, 'cod_pelicula' => $model->cod_pelicula], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'cod_espectador' => $model->cod_espectador, 'cod_pelicula' => $model->cod_pelicula], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'cod_espectador',
            'cod_pelicula',
            'fecha_estreno',
        ],
    ]) ?>

</div>
