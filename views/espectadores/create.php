<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Espectadores */

$this->title = 'Create Espectadores';
$this->params['breadcrumbs'][] = ['label' => 'Espectadores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="espectadores-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
