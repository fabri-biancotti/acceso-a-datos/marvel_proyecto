<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Espectadores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="espectadores-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Espectadores', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'cod_espectador',
            'nombre',
            'pais',
            'nombre_cine',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
