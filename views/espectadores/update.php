<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Espectadores */

$this->title = 'Update Espectadores: ' . $model->cod_espectador;
$this->params['breadcrumbs'][] = ['label' => 'Espectadores', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cod_espectador, 'url' => ['view', 'id' => $model->cod_espectador]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="espectadores-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
