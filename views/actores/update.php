<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Actores */

$this->title = 'Update Actores: ' . $model->dni_actor;
$this->params['breadcrumbs'][] = ['label' => 'Actores', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->dni_actor, 'url' => ['view', 'id' => $model->dni_actor]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="actores-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
